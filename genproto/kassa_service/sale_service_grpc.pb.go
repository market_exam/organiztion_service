// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.6.1
// source: sale_service.proto

package kassa_service

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// SaleServiceClient is the client API for SaleService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SaleServiceClient interface {
	Create(ctx context.Context, in *CreateSale, opts ...grpc.CallOption) (*Sale, error)
	GetById(ctx context.Context, in *SalePrimaryKey, opts ...grpc.CallOption) (*Sale, error)
	GetList(ctx context.Context, in *GetListSaleRequest, opts ...grpc.CallOption) (*GetListSaleResponse, error)
	Update(ctx context.Context, in *UpdateSale, opts ...grpc.CallOption) (*Sale, error)
	Delete(ctx context.Context, in *SalePrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error)
}

type saleServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSaleServiceClient(cc grpc.ClientConnInterface) SaleServiceClient {
	return &saleServiceClient{cc}
}

func (c *saleServiceClient) Create(ctx context.Context, in *CreateSale, opts ...grpc.CallOption) (*Sale, error) {
	out := new(Sale)
	err := c.cc.Invoke(ctx, "/kassa_service.SaleService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *saleServiceClient) GetById(ctx context.Context, in *SalePrimaryKey, opts ...grpc.CallOption) (*Sale, error) {
	out := new(Sale)
	err := c.cc.Invoke(ctx, "/kassa_service.SaleService/GetById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *saleServiceClient) GetList(ctx context.Context, in *GetListSaleRequest, opts ...grpc.CallOption) (*GetListSaleResponse, error) {
	out := new(GetListSaleResponse)
	err := c.cc.Invoke(ctx, "/kassa_service.SaleService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *saleServiceClient) Update(ctx context.Context, in *UpdateSale, opts ...grpc.CallOption) (*Sale, error) {
	out := new(Sale)
	err := c.cc.Invoke(ctx, "/kassa_service.SaleService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *saleServiceClient) Delete(ctx context.Context, in *SalePrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/kassa_service.SaleService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SaleServiceServer is the server API for SaleService service.
// All implementations must embed UnimplementedSaleServiceServer
// for forward compatibility
type SaleServiceServer interface {
	Create(context.Context, *CreateSale) (*Sale, error)
	GetById(context.Context, *SalePrimaryKey) (*Sale, error)
	GetList(context.Context, *GetListSaleRequest) (*GetListSaleResponse, error)
	Update(context.Context, *UpdateSale) (*Sale, error)
	Delete(context.Context, *SalePrimaryKey) (*empty.Empty, error)
	mustEmbedUnimplementedSaleServiceServer()
}

// UnimplementedSaleServiceServer must be embedded to have forward compatible implementations.
type UnimplementedSaleServiceServer struct {
}

func (UnimplementedSaleServiceServer) Create(context.Context, *CreateSale) (*Sale, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedSaleServiceServer) GetById(context.Context, *SalePrimaryKey) (*Sale, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedSaleServiceServer) GetList(context.Context, *GetListSaleRequest) (*GetListSaleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedSaleServiceServer) Update(context.Context, *UpdateSale) (*Sale, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedSaleServiceServer) Delete(context.Context, *SalePrimaryKey) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedSaleServiceServer) mustEmbedUnimplementedSaleServiceServer() {}

// UnsafeSaleServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SaleServiceServer will
// result in compilation errors.
type UnsafeSaleServiceServer interface {
	mustEmbedUnimplementedSaleServiceServer()
}

func RegisterSaleServiceServer(s grpc.ServiceRegistrar, srv SaleServiceServer) {
	s.RegisterService(&SaleService_ServiceDesc, srv)
}

func _SaleService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateSale)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kassa_service.SaleService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleServiceServer).Create(ctx, req.(*CreateSale))
	}
	return interceptor(ctx, in, info, handler)
}

func _SaleService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SalePrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kassa_service.SaleService/GetById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleServiceServer).GetById(ctx, req.(*SalePrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _SaleService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListSaleRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kassa_service.SaleService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleServiceServer).GetList(ctx, req.(*GetListSaleRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SaleService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateSale)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kassa_service.SaleService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleServiceServer).Update(ctx, req.(*UpdateSale))
	}
	return interceptor(ctx, in, info, handler)
}

func _SaleService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SalePrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SaleServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/kassa_service.SaleService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SaleServiceServer).Delete(ctx, req.(*SalePrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// SaleService_ServiceDesc is the grpc.ServiceDesc for SaleService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var SaleService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "kassa_service.SaleService",
	HandlerType: (*SaleServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _SaleService_Create_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _SaleService_GetById_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _SaleService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _SaleService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _SaleService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "sale_service.proto",
}
