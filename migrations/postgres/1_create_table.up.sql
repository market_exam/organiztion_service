CREATE TABLE IF NOT EXISTS "filials" (
    "id"           UUID PRIMARY KEY,
    "filial_code"  VARCHAR(50) NOT NULL,
    "name"         VARCHAR(50) NOT NULL,
    "address"      VARCHAR(50) NOT NULL,
	"phone_number" VARCHAR(50) NOT NULL,
    "created_at"   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMP,
    "deleted_at"   TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "couriers" (
    "id"           UUID PRIMARY KEY,
    "first_name"   VARCHAR(50) NOT NULL,
    "last_name"    VARCHAR(50) NOT NULL,
    "phone_number" VARCHAR(50) NOT NULL,
	"active"       BOOLEAN,
    "created_at"   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMP,
    "deleted_at"   TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "employees" (
    "id"           UUID PRIMARY KEY,
    "filial_id"    UUID  NOT NULL REFERENCES filials(id),
	"sale_point_id" UUID NOT NULL,
    "first_name"   VARCHAR(50) NOT NULL,
    "last_name"    VARCHAR(50) NOT NULL,
    "phone_number" VARCHAR(50) NOT NULL,
    "created_at"   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMP,
    "deleted_at"   TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "salepoints" (
    "id"           UUID PRIMARY KEY,
    "filial_id"    UUID  NOT NULL REFERENCES filials(id),
    "name"         VARCHAR(50) NOT NULL,
    "created_at"   TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at"   TIMESTAMP,
    "deleted_at"   TIMESTAMP
);

SELECT
	COUNT(*) OVER(),
	id,
	filial_code,
	name,
	address,
	phone_number,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "filials"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 

SELECT
	COUNT(*) OVER(),
	id,
	first_name,
	last_name,
	phone_number,
	active,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "couriers"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 

SELECT
	COUNT(*) OVER(),
	id,
	filial_id,
    sale_point_id,
	first_name,
	last_name,
	phone_number,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "employees"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 

SELECT
	COUNT(*) OVER(),
	id,
	filial_id,
	name,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "salepoints"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 