package storage

import (
	pb "app/genproto/organization_service"
	"context"
)

type StorageI interface {
	Filial() FilialRepoI
	Employee() EmployeeRepoI
	Courier() CourierRepoI
	SalePoint() SalePointRepoI
}

type FilialRepoI interface {
	Create(ctx context.Context, req *pb.CreateFilial) (resp *pb.FilialPrimaryKey, err error)
	GetAll(ctx context.Context, req *pb.GetListFilialRequest) (resp *pb.GetListFilialResponse, err error)
	GetById(ctx context.Context, req *pb.FilialPrimaryKey) (*pb.Filial, error)
	Update(ctx context.Context, req *pb.UpdateFilial) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *pb.FilialPrimaryKey) error
}

type CourierRepoI interface {
	Create(ctx context.Context, req *pb.CreateCourier) (resp *pb.CourierPrimaryKey, err error)
	GetAll(ctx context.Context, req *pb.GetListCourierRequest) (*pb.GetListCourierResponse, error)
	GetById(ctx context.Context, req *pb.CourierPrimaryKey) (*pb.Courier, error)
	Update(ctx context.Context, req *pb.UpdateCourier) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *pb.CourierPrimaryKey) error
}

type SalePointRepoI interface {
	Create(ctx context.Context, req *pb.CreateSalePoint) (resp *pb.SalePointPrimaryKey, err error)
	GetAll(ctx context.Context, req *pb.GetListSalePointRequest) (*pb.GetListSalePointResponse, error)
	GetById(ctx context.Context, req *pb.SalePointPrimaryKey) (*pb.SalePoint, error)
	Update(ctx context.Context, req *pb.UpdateSalePoint) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *pb.SalePointPrimaryKey) error
}

type EmployeeRepoI interface {
	Create(ctx context.Context, req *pb.CreateEmployee) (resp *pb.EmployeePrimaryKey, err error)
	GetAll(ctx context.Context, req *pb.GetListEmployeeRequest) (*pb.GetListEmployeeResponse, error)
	GetById(ctx context.Context, req *pb.EmployeePrimaryKey) (*pb.Employee, error)
	Update(ctx context.Context, req *pb.UpdateEmployee) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *pb.EmployeePrimaryKey) error
}



