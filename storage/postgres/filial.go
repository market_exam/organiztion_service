package postgres

import (
	"app/pkg/helper"
	"app/storage"
	"context"
	"database/sql"

	pb "app/genproto/organization_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type filialRepo struct {
	db *pgxpool.Pool
}

func NewFilialRepo(db *pgxpool.Pool) storage.FilialRepoI {
	return &filialRepo{
		db: db,
	}
}

func (r *filialRepo) Create(ctx context.Context, req *pb.CreateFilial) (resp *pb.FilialPrimaryKey, err error) {

	var id = uuid.New()

	query := `
		INSERT INTO "filials" (
			id,
			filial_code,
			name,
			address,
			phone_number,
			updated_at
		) 
		 VALUES ($1, $2, $3, $4, $5, now())
	`

	_, err = r.db.Exec(
		ctx,
		query,
		id.String(),
		req.FilialCode,
		req.Name,
		req.Address,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &pb.FilialPrimaryKey{Id: id.String()}, nil
}

func (r *filialRepo) GetById(ctx context.Context, req *pb.FilialPrimaryKey) (resp *pb.Filial, err error) {
	query := `
		SELECT 	
			id,
			filial_code,
			name,
			address,
			phone_number,
			created_at,
			updated_at
		FROM "filials"
		WHERE id = $1
	`

	var (
		filial_id      sql.NullString
		filial_code    sql.NullString
		filial_name    sql.NullString
		filial_address sql.NullString
		phone_number   sql.NullString
		createdAt      sql.NullString
		updatedAt      sql.NullString
	)

	err = r.db.QueryRow(ctx, query, req.Id).Scan(
		&filial_id,
		&filial_code,
		&filial_name,
		&filial_address,
		&phone_number,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &pb.Filial{
		Id:          filial_id.String,
		FilialCode:  filial_code.String,
		Name:        filial_name.String,
		Address:     filial_address.String,
		PhoneNumber: phone_number.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (r *filialRepo) GetAll(ctx context.Context, req *pb.GetListFilialRequest) (resp *pb.GetListFilialResponse, err error) {
	resp = &pb.GetListFilialResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		    SELECT
				COUNT(*) OVER(),
				id,
			    filial_code,
				name,
				address,
				phone_number,
				TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
				TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
			FROM "filials"
			WHERE true
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := r.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()
	for rows.Next() {

		var filial pb.Filial

		err = rows.Scan(
			&resp.Count,
			&filial.Id,
			&filial.FilialCode,
			&filial.Name,
			&filial.Address,
			&filial.PhoneNumber,
			&filial.CreatedAt,
			&filial.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Filiales = append(resp.Filiales, &filial)
	}

	return
}

func (r *filialRepo) Update(ctx context.Context, req *pb.UpdateFilial) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "filials"
			SET
				filial_code = :filial_code,
				name = :name,
				address = :address,
				phone_number = :phone_number,
				updated_at = now()
			WHERE
				id = :filial_id`
	params = map[string]interface{}{
		"filial_id":   req.GetId(),
		"filial_code": req.GetFilialCode(),
		"name":        req.GetName(),
		"address":     req.GetAddress(),
		"phone_number": req.GetPhoneNumber(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *filialRepo) Delete(ctx context.Context, req *pb.FilialPrimaryKey) error {
	
	query := `DELETE FROM "filials" WHERE id = $1`

	_, err := r.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
