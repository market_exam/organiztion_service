package postgres

import (
	"app/pkg/helper"
	"app/storage"
	"context"
	"database/sql"

	pb "app/genproto/organization_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type salepointRepo struct {
	db *pgxpool.Pool
}

func NewSalePointRepo(db *pgxpool.Pool) storage.SalePointRepoI {
	return &salepointRepo{
		db: db,
	}
}

func (c *salepointRepo) Create(ctx context.Context, req *pb.CreateSalePoint) (resp *pb.SalePointPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "salepoints" (
				id,
				filial_id,
				name,
				updated_at
			) VALUES ($1, $2, $3, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.FilialId,
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &pb.SalePointPrimaryKey{Id: id.String()}, nil
}

func (c *salepointRepo) GetById(ctx context.Context, req *pb.SalePointPrimaryKey) (resp *pb.SalePoint, err error) {

	query := `
		SELECT
			id,
			filial_id,
			name,
			created_at,
			updated_at,
			deleted_at
		FROM "salepoints"
		WHERE id = $1 and deleted_at is null
	`

	var (
		id         sql.NullString
		filial_id  sql.NullString
		name       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
		deleted_at sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&filial_id,
		&name,
		&created_at,
		&updated_at,
		&deleted_at,
	)

	if err != nil {
		return resp, err
	}

	resp = &pb.SalePoint{
		Id:        id.String,
		FilialId:  filial_id.String,
		Name:      name.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
		DeletedAt: deleted_at.String,
	}

	return
}

func (c *salepointRepo) GetAll(ctx context.Context, req *pb.GetListSalePointRequest) (resp *pb.GetListSalePointResponse, err error) {

	resp = &pb.GetListSalePointResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			filial_id,
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "salepoints"
		WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var salepoint pb.SalePoint

		err := rows.Scan(
			&resp.Count,
			&salepoint.Id,
			&salepoint.FilialId,
			&salepoint.Name,
			&salepoint.CreatedAt,
			&salepoint.UpdatedAt,
			&salepoint.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Salespoints = append(resp.Salespoints, &salepoint)
	}

	return
}

func (c *salepointRepo) Update(ctx context.Context, req *pb.UpdateSalePoint) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "salepoints"
			SET
				filial_id = :filial_id,
				name      = :name,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"filial_id": req.GetFilialId(),
		"name":      req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *salepointRepo) Delete(ctx context.Context, req *pb.SalePointPrimaryKey) error {

	query := `DELETE FROM "salepoints" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
