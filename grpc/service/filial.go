package service

import (
	"app/config"
	pb "app/genproto/organization_service"
	"app/pkg/logger"
	"app/storage"
	"context"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type filialService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	pb.UnimplementedFilialServiceServer
}

func NewFilialService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *filialService {
	return &filialService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (i *filialService) Create(ctx context.Context, req *pb.CreateFilial) (resp *pb.Filial, err error) {

	i.log.Info("---CreateFilial------>", logger.Any("req", req))

	pKey, err := i.strg.Filial().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateFilial->Filial->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Filial().GetById(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByIdFilial->Filial->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *filialService) GetByID(ctx context.Context, req *pb.FilialPrimaryKey) (resp *pb.Filial, err error) {

	fmt.Println("ok")

	c.log.Info("---GetFilialByID------>", logger.Any("req", req))

	resp, err = c.strg.Filial().GetById(ctx, req)
	if err != nil {
		c.log.Error("!!!GetFilialByID->Filial->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *filialService) GetList(ctx context.Context, req *pb.GetListFilialRequest) (resp *pb.GetListFilialResponse, err error) {

	i.log.Info("---GetFiliales------>", logger.Any("req", req))

	resp, err = i.strg.Filial().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetFiliales->Filial->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *filialService) Update(ctx context.Context, req *pb.UpdateFilial) (resp *pb.Filial, err error) {

	i.log.Info("---UpdateFilial------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Filial().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateFilial--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Filial().GetById(ctx, &pb.FilialPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetFilial->Filial->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *filialService) Delete(ctx context.Context, req *pb.FilialPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteFilial------>", logger.Any("req", req))

	err = i.strg.Filial().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteFilial->Filial->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
