package service

import (
	"app/config"
	pb "app/genproto/organization_service"
	"app/pkg/logger"
	"app/storage"
	"context"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type salepointService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	pb.UnimplementedSalePointServiceServer
}

func NewSalePointService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *salepointService {
	return &salepointService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (i *salepointService) Create(ctx context.Context, req *pb.CreateSalePoint) (resp *pb.SalePoint, err error) {

	i.log.Info("---CreateSalePoint------>", logger.Any("req", req))

	pKey, err := i.strg.SalePoint().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSalePoint->SalePoint->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SalePoint().GetById(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByIdSalePoint->SalePoint->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *salepointService) GetByID(ctx context.Context, req *pb.SalePointPrimaryKey) (resp *pb.SalePoint, err error) {

	fmt.Println("ok")

	c.log.Info("---GetSalePointByID------>", logger.Any("req", req))

	resp, err = c.strg.SalePoint().GetById(ctx, req)
	if err != nil {
		c.log.Error("!!!GetSalePointByID->SalePoint->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *salepointService) GetList(ctx context.Context, req *pb.GetListSalePointRequest) (resp *pb.GetListSalePointResponse, err error) {

	i.log.Info("---GetSalePointes------>", logger.Any("req", req))

	resp, err = i.strg.SalePoint().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSalePointes->SalePoint->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *salepointService) Update(ctx context.Context, req *pb.UpdateSalePoint) (resp *pb.SalePoint, err error) {

	i.log.Info("---UpdateSalePoint------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SalePoint().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSalePoint--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SalePoint().GetById(ctx, &pb.SalePointPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSalePoint->SalePoint->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *salepointService) Delete(ctx context.Context, req *pb.SalePointPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteSalePoint------>", logger.Any("req", req))

	err = i.strg.SalePoint().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSalePoint->SalePoint->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
